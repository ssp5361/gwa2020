import os,sys
import numpy as np
import math

import matplotlib.pyplot as plt


# Physical constants
c = 3.0E10 # cm
a = 7.5657E-15 # cgs
sigma = 5.6704E-5 # cgs

# Optical depth
def tau(Mej,Rej,kappa_opt):
    tau = 3.0/4.0/3.14 * kappa_opt * Mej / Rej**2
    return tau

# Radioactive luminosity
def Lr(Mej,t):
    L = 2.0E10 * Mej * t**(-1.3)
    return L


##################################
# Parameters for NS merger
##################################
Mej = 0.01 # Msun
vej = 30000 # km/s
kappa_opt = 1.0 # cm^2 g^-1
f = open("kilonova_blue.dat","w")
##################################


# initial values
Eint0 = 0.0  
t0 = 0.01 # days

# Conversion to cgs
Mej = Mej * 2.0E33  # g
vej = vej * 1.0E5  # cm/s


# Simple integration (Euler)
dt = 0.01 # days

# For plot
time = []
lum_kn = []
lum_rad = []
lum_dep = []

for i in range(10000):

    if(i == 0):
        Eint = Eint0
        
    tday = dt * (i+1) # day
    tsec = tday * 86400.0 # sec
    print "step: ", i, "tday: ", tday 
    
    # radius
    Rej = vej * tsec # cm
    # optical depth 
    tau_opt = tau(Mej,Rej,kappa_opt) 
    # deposition fraction (just a representative value)
    fdep = 0.5
    # escape time 
    tesc = tau_opt * Rej / c # s
    # luminosity
    Lkn = Eint / tesc # erg/s
    # radioactive luminosity
    Lrad = Lr(Mej,tday)    

    # basic eq.
    dEdt = -Lkn - Eint/tsec + fdep*Lrad

    # Output
    if(i != 0):
        # output
        line = "%15.5e %15.5e %15.5e %15.5e \n" % (tday, Lkn, Lrad, Eint)
        f.write(line)

        # for plot
        time.append(tday)
        lum_kn.append(Lkn)
        lum_rad.append(Lrad)
        lum_dep.append(fdep*Lrad)                
        
    # update
    Eint += dEdt * (dt*86400.)




# Plot
plt.xlabel("Time (day)")
plt.ylabel("Luminosity (erg/s)")
plt.tick_params(which='both', direction='in',bottom=True, top=True, left=True, right=True)

plt.plot(time,lum_kn, alpha=1, color="red", label="Lkn")
plt.plot(time,lum_rad, alpha=1, color="black", label="Lrad")
plt.plot(time,lum_dep, alpha=1, color="black",linestyle = "dashed", label="Ldep")
plt.xlim(0.0,30.0)
plt.ylim(1.0E39,1.0E42)
plt.yscale("log")

plt.minorticks_on()
plt.legend(loc='upper right') 
plt.show()


