# Masaomi Tanaka (Tohoku University)
The set of lecture notes and other useful python scripts used for the summer school on Kilonova light curve modeling are in these folders or one 
can be find them at:
https://www.astr.tohoku.ac.jp/~masaomi.tanaka/icts2020/