# ICTS SUMMER SCHOOL ON GRAVITATIONAL WAVE ASTROPHYSICS 2020 #

This year's [summer school](https://www.icts.res.in/program/gws2020) with theme as `Multi-Messeneger modeling of compact binary mergers`, contains four graduate level courses on Numerical Relativity, Numerical Hydrodynamics, Short Gamma ray bursts and Kilonova

Apart from with this set of lectures, this [2013 ICTS summer school](https://www.youtube.com/watch?v=t3uo2R-yu4o&list=PL04QVxpjcnjh2mFtM-RsRmbcWhETPcasv) on Numerical relativity, hydrodynamics, theory of PDE and advanced PDE
and these talks on [Gravitational wave astrophysics](https://www.youtube.com/watch?v=rRgMwZJu0_M&list=PLlQd-wufT12_kVe1QoviWj7P71wC5plb8) will complement the current course and learning.

Also refer to [ISSAA 2020](https://www.youtube.com/channel/UCy6pceaKQTxOLPStgzJ4aSg) for more video lectures on relevant astrophysical concepts
# Course content: #
# Numerical relativity #

[Harald Pfeiffer](https://www.aei.mpg.de/person/54205/2168) `(Max Planck Institute for Gravitational Physics)`

* Formalism:  3+1 decomposition,  solving constraints, formulations of evolution equations.
* Numerical implementation:  finite-differences, spectral methods, discontinuous Galerkin.
* Survey of BBH results. 

`References:` 

Bernd Brügmann, [Fundamentals of numerical relativity for gravitational wave sources, Science. 361, 6400, 366-371 (2018)](https://science.sciencemag.org/content/361/6400/366/tab-pdf). [Preparatory reading]

Luis Lehner and Frans Pretorius, [Numerical Relativity and Astrophysics, Annual Review of Astronomy and Astrophysics, 52:661-694 (2014)](https://www.annualreviews.org/doi/abs/10.1146/annurev-astro-081913-040031). [Preparatory reading]

Thomas W. Baumgarte, Stuart L. Shapiro, [Numerical Relativity: Solving Einstein's Equations on the Computer (Cambridge, 2010)](https://www.cambridge.org/core/books/numerical-relativity/72D4F6D791BC6F8F9CF87A60FC354D6A) 

Eric Gourgoulhon, [3+1 Formalism and Bases of Numerical Relativity.](https://arxiv.org/abs/gr-qc/0703035)
 
# Numerical hydrodynamics #

[Ian Hawke](https://cmg.soton.ac.uk/people/ih3/) `(University of Southampton)`

* Modelling assumptions for BNS merger simulations. 
* Conservation laws, shocks, equations of motion. 
* Finite volume methods. 
* MHD and methods. 
* Quick introduction to discontinuous Galerkin method, radiation, non-ideal MHD. 

`References: `

Font, J.A. [Numerical Hydrodynamics and Magnetohydrodynamics in General Relativity,  Living Rev. Relativ. 11, 7 (2008)](https://link.springer.com/article/10.12942%2Flrr-2008-7). [Preparatory reading] 

[Open Astrophysics Bookshelf](https://github.com/Open-Astrophysics-Bookshelf)

[The Einstein Toolkit](https://einsteintoolkit.org/)

[NRpy+](http://astro.phys.wvu.edu/bhathome/nrpy.html)

Leveque, [Finite Volume Methods for Hyperbolic Problems, (Cambridge, 2002)](https://www.cambridge.org/core/books/finite-volume-methods-for-hyperbolic-problems/97D5D1ACB1926DA1D4D52EAD6909E2B9)

Hesthaven, [Numerical Methods for Conserva on Laws: From Analysis to Algorithms, (SIAM, 20'8)](https://epubs.siam.org/doi/book/10.1137/1.9781611975109)

Rezzolla & Zanotti, [Relativistic Hydrodynamics (Oxford, 2013)](https://global.oup.com/academic/product/relativistic-hydrodynamics-9780198528906?cc=jp&lang=en&)

Andersson, [Gravitational Wave Astronomy (Oxford, 2019)](https://global.oup.com/academic/product/gravitational-wave-astronomy-9780198568032?cc=jp&lang=en&) 

Shibata, M., Taniguchi, K., [Coalescence of Black Hole-Neutron Star Binaries,  Living Rev. Relativ. 14, 6 (2011)](https://link.springer.com/article/10.12942%2Flrr-2011-6)

A full reading list is [here](https://github.com/IanHawke/icts-2020/blob/master/pdfs/reading_list.pdf). Will put up a repository of exercises (including coding exercises) which can be done as-and-when, but setting up for the coding exercises could be done in advance. 

See [this](https://github.com/IanHawke/icts-2020) github repository. The actual list of exercises is [here](https://github.com/IanHawke/icts-2020/blob/master/pdfs/exercises.pdf).  Slides themselves will be available [here](http://ianhawke.github.io/slides/icts-2020/index.html#/).

# Physics & astrophysics of gamma-ray bursts #

[Frédéric Daigne](http://www2.iap.fr/users/daigne/FD_IAP/Home.html) `(Institute of Astrophysics Paris)`

* Introduction: Brief review of important observational facts, Basic constraints on any GRB model: compact source + relativistic ejecta
* Theory: Progenitor - central engine - relativistic ejection (brief discussion), Modeling the prompt emission: internal dissipation in relativistic ejecta, Modeling the afterglow: interaction of relativistic ejecta with its environment, Modeling GRB population(s). 
* Focus on the short GRB - binary neutron star merger connection and GW170817

`References:`

Short review: [The Theory of Gamma-Ray Bursts](https://link.springer.com/article/10.1007/s11214-017-0423-z), Daign, Daigne & Meszaros, Space Science Review, 212, 409 (2017) [Preparatory reading] 

Detailed review: [The Physics of Gamma-Ray Bursts](https://arxiv.org/pdf/astro-ph/0405503.pdf), Piran, Rev. Mod. Phys. 76, 1143 (2004). 

Review on central engine - relativistic ejection - jet physics with a focus on the role of magnetic fields: [Gamma-Ray Bursts as a Source of Strong Magnetic Fields](https://link.springer.com/article/10.1007/s11214-015-0191-6), Granot, Piran, Bromberg, Racusin & Daigne, Space Science Review, 191, 471 (2015). 

Short GRBs and merger connection: [Short-Duration Gamma-Ray Bursts](https://arxiv.org/pdf/1311.2603.pdf), Berger, Annual Review of Astronomy & Astrophysics, 52, 43 (2014. 
 
# Physics & astrophysics of kilonovae #

[Masaomi Tanaka](https://www.astr.tohoku.ac.jp/~masaomi.tanaka/index_e.html) `(Tohoku University)`

* Brief review of relevant physics/observations: Mass ejection from NS merger, r-process nucleosynthesis, Observations of kilonova. 
* Physics of kilonova: Radioactive decays, Thermalization processes, Photon transfer, Atomic opacities

`References:`

Rosswog S., [The multi-messenger picture of compact binary mergers](https://arxiv.org/abs/1501.02081), International Journal of Modern Physics D, 24, 1530012 (2015). [Preparatory reading] 

Tanaka M., [Kilonova/Macronova Emission from Compact Binary Mergers](https://arxiv.org/abs/1605.07235), Advances in Astronomy, 2016, 634197 (2016). 

Fernandez R., Metzger B. D., [Electromagnetic Signatures of Neutron Star Mergers in the Advanced LIGO Era](https://arxiv.org/abs/1512.05435), Annual Review of Nuclear and Particle Science, 66, 23 (2016). 

Metzger, B.D. [Kilonovae](https://link.springer.com/article/10.1007%2Fs41114-017-0006-z). Living Rev Relativ 20, 3 (2017). 